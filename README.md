# Documentación de TeknoKasa

Accesible en [https://rosa.frama.io/teknokasa](https://rosa.frama.io/teknokasa)

## Licencia

Código con AGPLv3 y contenido con CC BY-NC-SA 4.0