---
weight: 0
title: "Glosario"
description: "Definición de términos"
icon: "article"
date: "2024-04-19T15:11:05+02:00"
lastmod: "2024-04-19T15:11:05+02:00"
draft: false
toc: true
---

Glosario ordenado alfabéticamente.

## Git

Git es un **sistema de control de versiones** distribuido, rápido y escalable que permite **gestionar y rastrear cambios en el código fuente** de un proyecto.

## GitLab 

GitLab es una **plataforma de gestión de proyectos y control de versiones** que utiliza el sistema de control de versiones Git.

Gitlab.com es la instancia de Gitlab Inc, una compañía de núcleo abierto y la principal proveedora del software GitLab.

## GitLab Pages

Es una **funcionalidad** del software de GitLab que **permite el alojamiento de páginas estáticas *gratuitamente y fácil de configurar***.

Se encarga de compilar el código y publicar el resultado de páginas estáticas.

## Hugo

[Hugo](https://gohugo.io) es un **generador de páginas estáticas** escrito en Go.

## Markdown

Es un lenguaje de marcado de texto y se usa como una **forma sencilla de agregar formato a textos web**.


## Modelo de núcleo abierto

> El modelo de núcleo abierto (open-core model) es un modelo de negocio para la monetización de software de código abierto producido comercialmente. **Implica** principalmente **ofrecer una versión "básica" o con funciones limitadas** de un **producto de software** como **software gratuito y de código abierto**, mientras que ofrece **versiones "comerciales" o complementos** como **software propietario**. 

[Wikipedia, Modelo de núbleo abierto](https://es.wikipedia.org/wiki/Modelo_de_n%C3%BAcleo_abierto)

## Repositorio de código

Un repositorio de código, también conocido como sistema de control de versiones o VCS (Version Control System), es un lugar donde se almacenan y se gestiona los archivos de código fuente de un proyecto, permitiendo a les desarrolladores colaborar y trabajar en el mismo proyecto de manera coordinada.

