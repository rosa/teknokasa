---
weight: 3
title: "¿Cómo crear una página web?"
description: "Guía de cómo crear una página web con Hugo"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo"
]
draft: false
toc: true
---

