---
weight: 3
title: "Generación de una página web con Hugo"
description: "¿Te interesa cómo crear una nueva página web con Hugo?"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo",
    "GitLab pages"
]
draft: false
toc: true
---

Sigue estos pasos:

## 1. Instalación de Hugo
[Instalar Hugo](https://gohugo.io/getting-started/installing/)<br>
{{< alert context="success" text="Recomendaciones: <strong>instalar hugo_extended</strong>" />}}
{{< tabs tabTotal="3">}}
{{% tab tabName="Linux" %}}

**Instalación de Hugo en Linux**

Si estás en Debian o Ubuntu puedes correr (aunque **sigue leyendo los siguientes párrafos**):

```bash
sudo apt-get install hugo
```

La versión de Hugo de los repositorios oficiales de Debian no es compatible con la versión de Hugo necesaria para el tema utilizado en la web. Por lo tanto, nos encontraríamos con el siguiente error al intentar ejecutar servir la web.

``` 
❯ hugo server -p 1312

WARN Module "XXXXX" is not compatible with this Hugo version; run "hugo mod graph" for more information.
Error: add site dependencies: load resources: loading templates: "~/YYYYY/themes/XXXXX/layouts/_default/_markup/render-codeblock-math.html:1:1": parse failed: template: _default/_markup/render-codeblock-math.html:1: unclosed action
```

Entonces, es necesario **instalar Hugo extended a través de la última [release](https://github.com/gohugoio/hugo/releases)**. La versión `_extended` se diferencia de la normal porque soporta SCSS. Una vez descargado el paquete adecuado para el sistema operativo y para la arquitectura de procesador de tu ordenador (se puede saber si es de 32 o 64 bits con el comando `lscpu`), utilizar el comando desde la carpeta con el comando instalado:

```bash
sudo dpkg -i hugo_extended_0.*_linux-amd64.deb
```

{{% /tab %}}
{{% tab tabName="Windows" %}}

**Instalación de Hugo en Windows**

Si estás con Windows y usas [Chocolatey](https://chocolatey.org/):

```
choco install hugo -confirm
```

{{% /tab %}}
{{% tab tabName="MacOS" %}}

**Instalación de Hugo en MacOS**

Si usas Mac asumiendo que tienes [Homebrew](https://brew.sh/):

```bash
brew install hugo
```

{{% /tab %}}
{{< /tabs >}}

## 2. Elección de tema 

Elegimos un [tema](https://themes.gohugo.io/). Por ejemplo, el tema de esta web [LotusDocs](https://themes.gohugo.io/themes/lotusdocs/).

## 3. Creación del repositorio remoto

Creamos el nuevo repo mediante la interfaz web y lo clonamos vacío mediante SSH. Para ello seguiremos los pasos explicados anteriormente en el apartado de [clonar repositorio en la guía editar con git](../editar/editar_con_git.md#1-clonar-el-repositorio). 

{{< alert context="info" text="Importarte: <strong>seguir los pasos para crear e importar claves SSH</strong>" />}}

Otra forma alternativa a este paso es inicializar del repositorio local y luego configurar el repositorio remoto. Por ejemplo en nuestro caso sería lo siguiente:

```
git init 
git remote add origin git@framagit.org:rosa/teknokasa.git
```

## 4. Creación del proyecto 

En este paso generamos el esqueleto de un proyecto de Hugo. Para ello, creamos un nuevo proyecto en una nueva carpeta llamada <nombre-proyecto> con:

```
hugo new site <name-project>
```

{{< alert context="info" text="En el caso de <strong>haber creado previamente una carpeta con el nombre del proyecto</strong>, ejecutaremos el siguiente comando:" />}}

```
hugo new site . -f
```

Donde el . es para referirse a la ruta relativa del sistema y -f para forzar la creación en un directorio que no está vacío.

## 5. Inicialización del repositorio remoto como módulo de Hugo
   
Una vez creado, volvemos a la terminal e inicializamos hugo con la dirección del nuevo repo. 

```bash
hugo mod init framagit.org/$USUARIE/$NOMBRE
```

## 6. Añadimos el tema como módulo de hugo

Añadimos el [tema como módulo de hugo](https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme). Con los siguientes comandos: 

```bash
hugo mod get github.com/colinwilson/lotusdocs
```    

## 7. Edición de la configuración

Editamos el fichero de configuración `config.toml` y añadimos:

```bash
baseURL = "http://localhost:1312/"
[module]
    [[module.imports]]
        path = "github.com/colinwilson/lotusdocs"
```

Es **altamente recomedable fijarse en el exampleSite del tema** para copiar y pegar el archivo **hugo.yaml** o **config.yaml**. Se puede cambiar la configuracion a un archivo **YAML** con un [convertidor online de TOML a YAML](https://www.convertsimple.com/convert-toml-to-yaml/). 

## 8. Servimos la página web

Servimos la página web con el siguiente comando y está disponible en la ruta [http://localhost:1312/](http://localhost:1312/).

```bash
hugo serve -p 1312
```

## 9. Creación de nuevo contenido

Creamos nuevo contenido en Hugo. Por ejemplo en el tema ReLearn, crearemos un nuevo capítulo con el siguiente comando:

```
hugo new --kind chapter hugo/_index.md
```

O si queremos crear una nueva entrada, escribimos:

```
hugo new hugo/quick_start.md
```

{{< alert context="success" text="Llegado a este punto, es recomendable volver a la documentación oficial para leerse la <strong>[estructura de directorios](https://gohugo.io/getting-started/directory-structure/) y la [configuración de Hugo](https://gohugo.io/getting-started/configuration/).</strong>" />}}

También es importante fijarnos en la configuración del tema elegido y el sitio de ejemplo asociado.

## 10. Publicamos los cambios

Publicamos el proyecto en GitLab. Para este paso es importante haber creado el repositorio remoto como se explica en el paso 3. Si lo hemos realizado correctamente, al ejecutar el comando `git remote -v` nos mostrará el repositorio remoto. Si nos 

- Añadimos todos los cambios a la fase stage:

```
git commit add .
```

- Nombramos y guardamos (commiteamos) la nueva versión:

```
git commit -m "Esqueleto creado"
``` 

- Subiremos los cambios de la rama main al repositorio remoto:

```
git push origin main
```
