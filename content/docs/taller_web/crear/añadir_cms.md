---
weight: 6
title: "Añadir un gestor de contenido"
description: "¿Quieres añadir un gestor de contenido para que usuaries no tecniques puedan gestionar la web?"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo",
    "GitLab pages"
]
draft: false
toc: true
---

Decap CMS (Content Management System) es un sistema de gestión de contenido. Hablando llanamente, una interfaz de administración web donde añadir contenido a un sitio web estático no requiera conocimientos técnicos. Prueba la [demo interactiva](https://demo.decapcms.org) y entenderás cómo funciona. Lo que diferencia a Decap CMS de otras soluciones comerciales es que es open source y es un proyecto dirigido por la comunidad.

Sveltia CMS es otra alternativa muy prometedora pero de momento está en fase beta.

Para crear una nueva web con el gestor de contenido Decap CMS incluido, seguiremos los siguientes pasos:

1. Crearemos una cuenta en GitLab.com y en Netlify.com. 
   
2. Nos dirigimos al repositorio [one-click-hugo-cms](https://github.com/decaporg/one-click-hugo-cms). Pulsamos en el botón Deploy to Netlify. Pulsaremos en connect to GitLab. Pondremos un nombre al repositorio que se creará en GitLab. Nos redigirá para que iniciemos sesión en GitLab y nos mostrará un mensaje de autorización confirmada. Si iniciamos sesión en GitLab veremos un mensaje donde nos dice si autorizamos a Netlify a usar nuestra cuenta. Pulsaremos que sí autorizamos. Nos redigirá a Netlify y veremos que hay una aplicación nueva con un nombre aleatorio. En GitLab veremos que hay un nuevo proyecto vacío con el nombre que hemos puesto anteriormente.

3. [Crearemos una nueva web](../crear/generacion_pagina_con_hugo.md) y la [desplegaremos en gitlab pages](../crear/despliegue_en_gitlab.md) con las guías anteriores. Es importante este paso para saber cuál es la URI de redirección en durante la configuración de Netlify.

4. Creamos un nuevo directorio `admin` en la carpeta `static`. En ella copiamos los archivos que aparecen en [https://framagit.org/rosa/teknokasa/-/tree/main/static/admin](https://framagit.org/rosa/teknokasa/-/tree/main/static/admin). Modificaremos el config.yml en función de la carpeta que queramos modificar.

4. En GitLab, iremos al icono de nuestro avatar -> Set Profile ->  Applications -> Add new application -> Nombraremos la aplicadión (por ejemplo con el nombre del proyecto + Decap CMS) -> Redirect URI es la URL de redirección a nuestro  

4. En Netlify, iremos al nuevo proyecto -> Site Configuration -> Access & Security - > OAuth - > Install Provider -> GitLab

