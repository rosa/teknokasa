---
weight: 4
title: "Despliegue con Gitlab pages"
description: "¿Sabes cómo desplegar un proyecto en GitLab pages?o"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo",
    "GitLab pages"
]
draft: false
toc: true
---

Siguiendo los siguientes pasos desplegaremos el proyecto en GitLab pages:

1. Configuramos la integración y despliegue contínuo creando el archivo `.gitlab-ci.yml` con la plantilla de Hugo. Si realizamos esto desde GitLab, en el contenido de la plantilla de GitLab, será necesario modificar la rama master por main.

    ```yaml
    default:
      image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo/hugo_extended:latest"

    variables:
      GIT_SUBMODULE_STRATEGY: recursive
      HUGO_ENV: production

    before_script:
      - apk add --no-cache go curl bash nodejs
      - hugo mod get -u
      ## Uncomment the following if you use PostCSS. See https://gohugo.io/hugo-pipes/postcss/
      #- npm install postcss postcss-cli autoprefixer

    test:
      script:
        - hugo
      rules:
        - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

    pages:
      script:
        - hugo
      artifacts:
        paths:
          - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      environment: production
    ```

2. Modificar la URL base que tiene esta estructura `baseURL = "https://<gitlab-user>.frama.io/<project-name>/"`. Si queremos acortalo a `https://pepito.frama.io`, podemos crear un grupo o usuario que se llame `pepito` y dentro, un repositorio que se llame `pepito.frama.io` 

3. Habilitar el acceso a todos los públicos a [GitLab](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html). Navegar en los ajustes del proyecto de GitLab y expandir Visibility, project features, permissions > Pages > Everyone.

