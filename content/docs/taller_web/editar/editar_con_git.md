---
weight: 2
title: "Editar en local con git"
description: "Guía para editar con git"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo",
    "Git"
]
draft: false
toc: true
---

Para seguir estos pasos **se requiere** conocimientos básicos del lenguaje de marcado Markdown y **una cuenta en [Framagit](https://framagit.org/users/sign_up)** [^1], que es la instancia de GitLab donde se aloja la página web. Además, se requieren conocimientos básicos de git, aunque si no los tienes, este puede ser un buen momento para aprender a utilizar este sistema de gestión de versiones.

{{< alert context="danger" text="Requisitos: <strong>registrarse en Framagit</strong>. Tardan entre 1 o 2 días en aceptar las nuevas cuentas." />}}

[^1]: Las nuevas cuentas registradas en framagit requieren ser aprobadas por la moderacion de framagit, lo cual puede demorar varios dias. Si quieres colaborar modificando la web y no quieres esperar la aprobacion de tu cuenta de framagit puedes solicitar acceso a una cuenta común.

<u>Brevemente lo que vamos a hacer es:</u>

* Crear una cuenta de [Framagit](https://framagit.org/users/sign_up).

* [Instalar Git](#0-instalar-git)

* [Clonar el repositorio de la web (para tenerlo en local y poder editar)](#1-clonar-el-repositorio)

* [Instalar Hugo](#2-instalar-hugo)

* [Servir la web y editar.](#editar)

* [Fusionar los cambios que hicimos con la rama (o versión) principal (en framagit) a traves de los issues y merge requests.](#fusionar-los-cambios)

### Editar un contenido o entrada ya creada

Pasos:

#### 0. Instalar Git

En su [web oficial](https://git-scm.com/downloads) veremos cómo instalarlo en función de nuestro sistema operativo.

#### 1. Clonar el repositorio.

Framagit es el repositorio remoto donde se encuentra el código de esta web. Lo puedes encontrar [aquí](https://framagit.org/rosa/teknokasa). Para poder editarlo en local, tenemos que clonar del repositorio remoto a nuestra compu. Para clonar repositorios, existen dos formas, mediante SSH o HTTPS. Lo podemos comprobar pinchando en el botón azul de `Código`. 

Se puede clonar mediante SSH, si previamente se ha subido la clave pública a framagit, o mediante HTTPS. Por seguridad, es recomendable realizarlo mediante SSH, debido a que la opción de HTTPS pide autenticarse con usuarie y contraseña cada vez que se quiera realizar algún cambio. En cambio, mediante SSH y `ssh-add`, sólo será necesario introducir una vez la contraseña SSH y la autenticación es más fuerte al hacerse por claves público-privadas.    

1. Crea tus claves ssh para autenticarte con Framagit a través de git. 

    ```bash
    ssh-keygen -o -a 100 -t ed25519 -f $HOME/.ssh/framagit.usuarie.ed25519 -C "usuarie"
    ```

2. Añade tu clave pública SSH (.pub) a tu cuenta de Framagit. Se encontrará en `$HOME/.ssh/framagit.usuarie.ed25519.pub`.

    - Abriremos con un editor la el archivo creado o mediante una terminal:
    
    ```bash
    cat ~/.ssh/framagit.usuarie.ed25519
    ```

    - Pincharemos en el icono de nuestra cuenta -> Preferencias -> SSH Keys -> Añadir nueva clave -> Copiaremos el contenido del fichero anterior y guardaremos.

    Por último, ejecutaremos el siguiente comando en una terminal para facilitarnos la subida de código local a Framagit en el paso de [fusionar los cambios](#fusionar-los-cambios).

    ```bash
    ssh-add ~/.ssh/framagit.usuarie.ed25519
    ```

3. Clona el repositorio por SSH:

    ```bash
    # SSH
    git clone git@framagit.org:rosa/teknokasa.git

    # HTTPS
    https://framagit.org/rosa/teknokasa.git
    ``` 

4. Añade un email y un nombre en la configuracion de git para los commits:
    ```bash
    git config --local user.name "Nombre de ejemplo"
    git config --local user.email "ejemplo@ejemplo.org"
    ``` 

#### 2. [Instalar Hugo](https://gohugo.io/getting-started/installing/)<br>
{{< alert context="success" text="Recomendaciones: <strong>instalar hugo_extended</strong>" />}}
{{< tabs tabTotal="3">}}
{{% tab tabName="Linux" %}}

**Instalación de Hugo en Linux**

Si estás en Debian o Ubuntu puedes correr (aunque **sigue leyendo los siguientes párrafos**):

```bash
sudo apt-get install hugo
```

La versión de Hugo de los repositorios oficiales de Debian no es compatible con la versión de Hugo necesaria para el tema utilizado en la web. Por lo tanto, nos encontraríamos con el siguiente error al intentar ejecutar servir la web.

``` 
❯ hugo server -p 1312

WARN Module "XXXXX" is not compatible with this Hugo version; run "hugo mod graph" for more information.
Error: add site dependencies: load resources: loading templates: "~/YYYYY/themes/XXXXX/layouts/_default/_markup/render-codeblock-math.html:1:1": parse failed: template: _default/_markup/render-codeblock-math.html:1: unclosed action
```

Entonces, es necesario **instalar Hugo extended a través de la última [release](https://github.com/gohugoio/hugo/releases)**. La versión `_extended` se diferencia de la normal porque soporta SCSS. Una vez descargado el paquete adecuado para el sistema operativo y para la arquitectura de procesador de tu ordenador (se puede saber si es de 32 o 64 bits con el comando `lscpu`), utilizar el comando desde la carpeta con el comando instalado:

```bash
sudo dpkg -i hugo_extended_0.*_linux-amd64.deb
```

{{% /tab %}}
{{% tab tabName="Windows" %}}

**Instalación de Hugo en Windows**

La opción más sencilla es **instalar Hugo extended a través de la última [release](https://github.com/gohugoio/hugo/releases)**.

O si estás con Windows y usas [Chocolatey](https://chocolatey.org/):

```
choco install hugo -confirm
```

{{% /tab %}}
{{% tab tabName="MacOS" %}}

**Instalación de Hugo en MacOS**

Si usas Mac asumiendo que tienes [Homebrew](https://brew.sh/):

```bash
brew install hugo
```

{{% /tab %}}

{{< /tabs >}}

#### Editar

1. **Servir la web** con `hugo serve -p 1312` y abrir [http://localhost:1312](http://localhost:1312) en el navegador.

2. **Editar el fichero**. Para editar un fichero ya creado iremos a la carpeta `content/` donde veremos las secciones de la barra de navegacion. Elegiremos el fichero que queremos editar y haremos los cambios con nuestro editor de código preferido. En caso de no tener un editor de código, recomendamos empezar por uno como [VSCodium](https://vscodium.com/), que es la versión libre y de código abierto de VS Code.

3. **Visualizar los cambios** en el navegador después de guardar el fichero modificado. **Comprobar que los cambios funcionan en local.**

#### Fusionar los cambios

Una vez que hayamos realizado cambios en el código y hayamos comprobado que todo funciona correctamente en local, procederemos a hacer un commit. Esto significa que queremos guardar una versión el código actual. Si es la primera vez que usamos git, recomendamos utilizar una interfaz de comandos como la pestaña de Source Control (ajato Ctrl+Shift+G) de VSCodium o [instalar algún programa de interfaz gráfica](https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Graphical-Interfaces). Para hacer un commit, pasaremos al estado stage el código que queramos guardar y pondremos un mensaje al commit. Para publicarlo pulsaremos el botón de la nube o Publish. En caso de querer hacerlo mediante la terminal, usaremos los siguientes comandos:

0. Ver que archivos han sido modificados:

    ```bash
    git status
    ```

1. Añadir al estado Stage:

    - Para añadir todos los cambios:
    
    ```bash
    git add .
    ```

    - Para añadir sólo un archivo, por ejemplo la plantilla:

    ```bash
    git add content/docs/taller_web/editar/plantilla_editar.md
    ```

2. Hacer un commit en la rama actual, que seguramente sea la main.

    ```bash
    git commit -m "texto descriptivo de los cambios realizados"
    ```

3. Publicarlo al repositorio remoto.

    ```bash
    git push origin main
    ```

5. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

Lo ideal es utilizar los [issues](https://framagit.org/hacklab/hacklab.frama.io/-/issues) y [merge requests](https://framagit.org/hacklab/hacklab.frama.io/-/merge_requests) para indicar qué cambio vamos a realizar en el código. Aunque al principio puede parecer un poco lioso, son buenas prácticas para la revisión de código y adminitración de tareas.

1. Primero abrimos un issue con un merge request asociado. En los issues planteamos que problema queremos resolver. Esto nos ayudará a focalizar y pensar la mejor solución al problema. Para ello, iremos a la ventana de los issues y crearemos uno nuevo con un nombre autodescriptivo. A continuación, seleccionaremos la opción de crear un merge request asociado y lo completaremos. Se habrá creado una nueva rama, que necesitaremos en los siguientes pasos. Por lo que la copiaremos y utilizaremos `git checkout -b <nombre-rama-creada-merge-request>` para editar sobre ella.

2. Después de comprobar que todo funciona, podremos ver los archivos modificados con `git status` y los cambios concretos con `git diff`. Para añadir los archivos que queramos actualizar al estado stage, ejecutamos `git add <nombre-archivo-a-actualizar>`, o `git add .` en caso de querer comitear todos los cambios realizados. Para pasar del estado stage al final, haremos un commit en la rama creada con `git commit -m "Mensaje descriptivo de los cambios"`. Para subir los cambios al repositorio origen usaremos `git push origin <nombre-rama-creada-merge-request>`. En este caso, se habrá creado un merge request que habrá que pasar del estado Draft al definitivo y aprobarlo para que se fusionen los cambios de la `<nombre-rama-creada-merge-request>` con la `main`. Si no hemos creado un merge request asociado, simplemente haremos un commit en la rama main y lo suberemos al repositorio de origen.

3. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Previamente, es necesario haber completado los pasos de la sección anterior. Una vez que estemos sirviendo la web con `hugo serve -p 1312` y la visualicemos con [http://localhost:1312](http://localhost:1312) en el navegador, seguiremos estos pasos:

1. Abrir el proyecto con el editor de código preferido y con una terminal. Si queremos crear un nuevo capítulo o sección con el nombre `nuevo`, ejecutamos lo siguiente:

    ```zsh
    hugo new --kind chapter nuevo/_index.md
    ```

    Si simplemente queremos crear una nueva entrada, escribimos:

    ```zsh
    hugo new hugo/quick_start.md
    ```

2. Después, ya se podrá visualizar los cambios en el navegador después de guardar el fichero modificado.

3. Hacer un commit en la rama main y subirlo a Framagit.

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.
