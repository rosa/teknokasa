---
weight: 2
title: "Editar desde un editor de código online"
description: "Guía para editar desde el editor de código integrado en GitLab"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
draft: false
toc: true
---

Para seguir estos pasos, se requiere conocimientos básicos del [lenguaje de marcado Markdown](https://es.wikipedia.org/wiki/Markdown)[^1] y una en cuenta en [Framagit](https://framagit.org) (instancia de GitLab donde se aloja la página web).

[^1]: [Wikipedia, Markdown](https://es.wikipedia.org/wiki/Markdown)

{{< alert context="danger" text="Requisitos: <strong>registrarse en Framagit</strong>. Tardan entre 1 o 2 días en aceptar las nuevas cuentas." />}}

### Editar un contenido o entrada ya creada

Pasos:

1. En la esquina inferior izquierda, existe un enlace con el nombre "Edit this page" que dirige al [Framagit](https://framagit.org) donde está alojado el código fuente de esta página web. **Pinchar en el enlace "Edit this page"**.

2. Como previamente nos hemos registrado en [Framagit](https://framagit.org), ahora **iniciamos sesión** en modo Standard con usuario y contraseña.

3. Modificar el contenido del fichero al que nos ha redirgido y pulsar "Commit changes". Para modificar el fichero, debemos seguir la sintaxis de Markdown[^2]. Existen muchos post en internet donde se explica la sintaxis de Markdown, uno de ellos es este [enlace](https://markdown.es/sintaxis-markdown/).

[^2]: [Sintaxis Markdown](https://markdown.es/sintaxis-markdown/)

4. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.

### Crear una entrada nueva en la web

Pasos:

1. **Iniciamos sesión en Framagit** en modo Standard con usuario y contraseña. Iremos al repositorio de la web que queramos editar, en nuestro caso [TeknoKasa Docs](https://framagit.org/rosa/teknokasa).

2. Una vez nos encontremos en el repositorio de la página web, pincharemos en Web IDE para abrir un entorno de desarrollo integrado en GitLab. Podremos observar la estructura de directorios de un proyecto creado con Hugo. Si se quiere saber más sobre esta estructura, la mejor opción es ir directamente a la [documentación de Hugo sobre la estructura de directorios](https://gohugo.io/getting-started/directory-structure/).

3. Nos desplazaremos al archivo `content/docs/taller_web/plantilla_editar.md` y copiaremos los metadatos que se muestran con la separación entre - - - (incluidas las separaciones).  

4. Una vez copiado, tendremos que decidir dónde crear la nueva entrada y si se trata de un capítulo nuevo o no. En caso de ser un nuevo capítulo, se requiere crear una nueva carpeta que contenga un archivo `_index.md` y en vez de copiar el archivo anterior, podremos copiar cualquier otro archivo `_index.md`. Una vez esté decido, crear un nuevo fichero con el nombre que se quiera y que termine en .md`. 

5. Pegamos al inicio del fichero los metadatos de la plantilla modificando lo que sea necesario (título, fecha, peso, ...). Seguimos añadiendo el contenido que queramos. 

6. Una vez esté terminado (o queramos guardar los cambios para comprobar que todo funciona correctamente), pulsaremos **"Create commit"** y **"Commit to main branch"**. Escribimos un breve mensaje con los cambios realizados y pulsamos **"Commit"**.

7. Esperar 30 segundos o un minuto a que termine de completarse la integración y despliegue contínuo (CI/CD) y refrescar la página web con "Ctrl + Shift + R" para recargar la página sin utilizar la caché del navegador. Si todo ha ido correctamente, se podrán visualizar los nuevos cambios.