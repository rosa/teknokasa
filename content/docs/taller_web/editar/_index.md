---
weight: -1
title: "¿Cómo editar esta web?"
description: "Guía de cómo editar esta web estática"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo"
]
draft: false
toc: true
---