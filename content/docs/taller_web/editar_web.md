---
weight: -5
title: "Guías para editar esta web"
description: "Guías paso a paso para editar esta web estática"
icon: "rocket_launch"
date: "2024-04-19T14:46:04+02:00"
lastmod: "2024-04-19T14:46:04+02:00"
tags: [
    "Web estática",
    "Hugo"
]
draft: false
toc: true
---

En esta sección se explica cómo contribuir o editar esta web estática generada con Hugo y desplegada con GitLab Pages. Existen diferentes maneras de editar la web, por lo que se exponen las diferentes formas en función de los conocimientos técnicos o ganas de aprender.

{{< alert context="danger" text="Requisitos: <strong>registrarse en Framagit</strong>. Tardan entre 1 o 2 días en aceptar las nuevas cuentas." />}}

## Edición de páginas web

En las siguientes dos guías se dividen en dos partes: i) editar un contenido o entrada ya creada, ii) crear una entrada nueva en la web.

- [**Demo para editar desde un navegador con un gestor de contenido**](https://demo.decapcms.org/). Es una demo de cómo nos permitiría editar Decap CMS.

- [**Editar desde un navegador con la interfaz gráfica de Framagit**](/docs/taller_web/editar/editar_desde_navegador). Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en Framagit (instancia de GitLab donde se aloja el código y la página web). 

- [**Editar en local con git**](/docs/taller_web/editar/editar_con_git). Se requiere conocimientos básicos del lenguaje de marcado Markdown y una cuenta en Framagit. Además, se requieren conocimientos de git y quitarse el miedo a usar la terminal (aunque también existen interfaces gráficas para git :) ).

## Creación de páginas web

En esta sección o capítulo se explican los primeros pasos para crear y desplegar una página web estática generada con Hugo en GitLab pages.

- [**Generación de una página web con Hugo**](/docs/taller_web/crear/generacion_pagina_con_hugo).

- [**Despliegue con Gitlab Pages**](/docs/taller_web/crear/despliegue_en_gitlab/).

- [**Añadir un gestor de contenido**](/docs/taller_web/crear/añadir_cms/)